import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileOutputStream;
import java.io.IOException;

public class ResumeGenerator {
String title;
Font title_font,text_font;
String[] TheSacredTexts;
ResumeGenerator(){
    title="Resume";
    TheSacredTexts=new String[10];
    title_font= new Font(Font.FontFamily.HELVETICA,32);
    text_font= new Font(Font.FontFamily.HELVETICA,14);
}
    public void createPdf(String filename)
            throws IOException, DocumentException {
        // step 1
        Document document = new Document();
        // step 2
        PdfWriter.getInstance(document, new FileOutputStream(filename));
        // step 3
        document.open();
        // step 4
        Paragraph preface = new Paragraph(title,title_font);
        preface.setAlignment(Element.ALIGN_CENTER);

        document.add(preface);
        document.add( Chunk.NEWLINE );
        document.add( Chunk.NEWLINE );
        InsertTexts(TheSacredTexts);
        document.add(createSymmetricalTable(2,5));
        // step 5
        document.close();
    }

    public PdfPTable createSymmetricalTable(int cols, int rows) {

        PdfPTable table = new PdfPTable(cols);

        PdfPCell cell;

        for (int i = 0; i < rows*cols; i++){
            Phrase phr=new Phrase(TheSacredTexts[i],text_font);

            cell = new PdfPCell(phr);
            cell.setRowspan(cols);
            cell.setLeading(0,1.5f);
            cell.setPaddingBottom(10);

            table.addCell(cell);
        }
        return table;
    }
    public void InsertTexts(String[] SacredTexts){
            SacredTexts[0]="First Name";
            SacredTexts[1]="Kamil";
        SacredTexts[2]="Last Name";
        SacredTexts[3]="Kapalka";
        SacredTexts[4]="Profession";
        SacredTexts[5]="Sporadic Coffee-to-Code Converter";
        SacredTexts[6]="Education";
        SacredTexts[7]="2015-present PWSZ in Tarnow\n2011-2015 ZSME in Tarnow";
        SacredTexts[8]="Summary";
        SacredTexts[9]="Familiar with a plethora of different programming languages and software. Producing code of unparalleled quality.";
    }
}
