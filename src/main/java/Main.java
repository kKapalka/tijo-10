import com.itextpdf.text.DocumentException;

import java.io.IOException;



public class Main {

    public static void main(String[] args)
    {
        ResumeGenerator rg=new ResumeGenerator();
        try {
            rg.createPdf("resume.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }
}
